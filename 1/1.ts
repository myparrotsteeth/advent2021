const fs = require('fs');

//let input = [199,200,208,210,200,207,240,269,260,263];
fs.readFile('input', function(err,data) {
  if(err) throw err;
  let input = data.toString().split("\n");
  let deltas = [];

  for(let i = 0; i < input.length; i++) {
    if (i != 0) {
      deltas.push(input[i]-input[i-1]);
    }
  }

  console.log("deltas count = " + deltas.length);
  let positiveCount = deltas.filter(x => x >= 0).length;

  console.log(positiveCount);
});
