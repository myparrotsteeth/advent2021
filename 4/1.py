#!/usr/bin/python

board_size = 5

def call_out(number,boardin):
    retval = [ ["toad" if value == number else value for value in line ] for line in boardin ]
    return retval

def has_won(boardin):
    return_val = False
    for line in boardin:
        return_val = (all(elem == "toad" for elem in line))
        if return_val:
            break
    return return_val

def transpose(in_board):
    return list(map(list, zip(*in_board)))

def get_score(in_board,shout):
    flattened_board = [ string for line in in_board for string in line ]
    parsed_board = [ int(string) if string != "toad" else 0 for string in flattened_board  ]

    #flattened_board = [ int(number) if number != 'toad' else 0 for number in line for line in in_board ]
    score = sum(parsed_board)*int(shout)
    return score

input = open("input", "r")
lines = input.readlines()

bingo_numbers = lines[0].strip().split(",")

boards = []

board_input = lines[2:]

current_board = []

for line in board_input:
    if line == '\n':
        boards.append(current_board)
        current_board = []

    else:
        board_line = line.strip().split()
        current_board.append(board_line)

if current_board != []:
    boards.append(current_board)

winning_board = []
winning_shout = None

for shout in bingo_numbers:

    boards = [ call_out(shout,board) for board in boards ]

    for board in boards:
        transposed = transpose(board)
        if has_won(board) or has_won(transposed):
            winning_board = board
            winning_shout = shout
            break

    if winning_board != []:
        break

print(shout)
print(winning_shout)

if winning_board != []:
    score = get_score(winning_board,winning_shout)
